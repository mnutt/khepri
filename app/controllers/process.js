import Controller from '@ember/controller';
import { set, get } from '@ember/object';

export default Controller.extend({
  follow: true,

  actions: {
    clear() {
      set(this, 'model.data', []);
    },

    toggleFollow() {
      this.toggleProperty('follow');
    },

    fireCommand() {
      this.send('fireCommandToProcess', get(this, 'model.command'));
      set(this, 'model.command', null);
    }
  }
});
